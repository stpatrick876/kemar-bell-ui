import { Component, AfterViewInit, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss', './hamburger.scss']
})
export class AppComponent implements OnInit {
  isHandsetLayout = false;
  isLoading = false;
  constructor(private breakpointObserver: BreakpointObserver, private loaderService: LoaderService) { }

  ngOnInit() {
    this.breakpointObserver.observe([
      Breakpoints.HandsetLandscape,
      Breakpoints.HandsetPortrait
    ]).subscribe(result => {
      this.isHandsetLayout = result.matches;
    });

    this.loaderService.loaderState.subscribe((state: { show: boolean; }) => {
      console.log(state);
      this.isLoading = state?.show;
    });
  }



}
