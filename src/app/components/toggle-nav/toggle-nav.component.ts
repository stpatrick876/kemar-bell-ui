import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { NavigationService, INavItem } from 'src/app/services/navigation.service';
declare const $: any;

@Component({
  selector: 'kb-dev-toggle-nav',
  templateUrl: './toggle-nav.component.html',
  styleUrls: ['./toggle-nav.component.scss']
})
export class ToggleNavComponent implements OnInit, AfterViewInit {
  navItems: INavItem[] = NavigationService.navItems;
  @Input() isHandsetLayout = false;
  @Input() isLoading = false;
  constructor(public navigationService: NavigationService) { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    // Navigation
    const body: HTMLElement = document.querySelector('body');
    const menu: HTMLElement = document.querySelector('.menu-icon');
    const nav: HTMLElement = document.querySelector('.nav');

    const toggleClass = (element: HTMLElement, stringClass: string) => {
      if (element.classList.contains(stringClass)) {
        element.classList.remove(stringClass);
      } else {
         element.classList.add(stringClass);
      }
    };

    menu.addEventListener('click', () => toggleClass(body, 'nav-active'));
    nav.addEventListener('mouseleave', () => body.classList.remove('nav-active'));
  }

  navigateTo(path: string) {
    this.navigationService.navigateTo(path);
  }

}
