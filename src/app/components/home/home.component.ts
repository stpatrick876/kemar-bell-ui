import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import RINGS from 'vanta/dist/vanta.rings.min';
import { NavigationService } from 'src/app/services/navigation.service';
@Component({
  selector: 'kb-dev-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isHandsetLayout: boolean;

  constructor(private breakpointObserver: BreakpointObserver, private navigationService: NavigationService) {   }

  ngOnInit(): void {
    this.breakpointObserver.observe([
      Breakpoints.HandsetLandscape,
      Breakpoints.HandsetPortrait
    ]).subscribe(result => {
      this.isHandsetLayout = result.matches;
    });

    RINGS({
      el: '.page-container--home',
      mouseControls: true,
      touchControls: true,
      minHeight: 200.00,
      minWidth: 200.00,
      scale: 1.00,
      scaleMobile: 1.00,
      backgroundColor: '#231f20',
      backgroundAlpha: 0.98
    });
  }

  navigateTo(path: string) {
    this.navigationService.navigateTo(path);
  }

}
