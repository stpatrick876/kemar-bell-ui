import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'kb-dev-name-graphic',
  templateUrl: './name-graphic.component.html',
  styleUrls: ['./name-graphic.component.scss']
})
export class NameGraphicComponent implements OnInit {
  @Input() text: string;
  constructor() { }

  ngOnInit(): void {
  }

}
