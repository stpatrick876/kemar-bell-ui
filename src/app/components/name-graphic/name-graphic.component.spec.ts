import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NameGraphicComponent } from './name-graphic.component';

describe('NameGraphicComponent', () => {
  let component: NameGraphicComponent;
  let fixture: ComponentFixture<NameGraphicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NameGraphicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NameGraphicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
