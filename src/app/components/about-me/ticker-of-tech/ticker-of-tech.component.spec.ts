import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TickerOfTechComponent } from './ticker-of-tech.component';

describe('TickerOfTechComponent', () => {
  let component: TickerOfTechComponent;
  let fixture: ComponentFixture<TickerOfTechComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TickerOfTechComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TickerOfTechComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
