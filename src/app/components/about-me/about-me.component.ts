import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';

@Component({
  selector: 'kb-dev-about-me',
  templateUrl: './about-me.component.html',
  styleUrls: ['./about-me.component.scss', './cube.scss']
})
export class AboutMeComponent implements OnInit {
  isHandsetLayout: boolean;

  constructor(private breakpointObserver: BreakpointObserver) { }

  ngOnInit(): void {
    this.breakpointObserver.observe([
      Breakpoints.HandsetLandscape,
      Breakpoints.HandsetPortrait
    ]).subscribe(result => {
      this.isHandsetLayout = result.matches;
    });
  }

}
