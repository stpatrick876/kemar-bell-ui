import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CubeOfTechComponent } from './cube-of-tech.component';

describe('CubeOfTechComponent', () => {
  let component: CubeOfTechComponent;
  let fixture: ComponentFixture<CubeOfTechComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CubeOfTechComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CubeOfTechComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
