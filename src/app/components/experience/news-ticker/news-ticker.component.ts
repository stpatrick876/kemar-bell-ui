import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'kb-dev-news-ticker',
  templateUrl: './news-ticker.component.html',
  styleUrls: ['./news-ticker.component.scss']
})
export class NewsTickerComponent implements OnInit {
  specializations: any[] = [
    {
      title: "User Interface",
      desc:
        "Fluent in vanilla js, es6, Modern UI Frameworks (Angular, React, Vue ...)",
      icons: [
        "devicon-angularjs-plain",
        "devicon-react-original-wordmark",
        "devicon-javascript-plain",
        "devicon-jquery-plain-wordmark",
        "devicon-sass-original",
        "devicon-typescript-plain"
      ]
    },
    {
      title: "Data Visualization",
      desc: "Capable of transforming data into Rich charts and Diagrams",
      icons: ["devicon-d3js-plain"]
    },
    {
      title: "Server Side",
      desc:
        "Famailiarity with working in with AWS Services, Serverless Frameworks",
      icons: [
        "devicon-nodejs-plain-wordmark",
        "devicon-java-plain-wordmark",
        "devicon-csharp-line-wordmark"
      ]
    },
    {
      title: "API",
      desc: "Design,  Creation and Consumption of REST, SOAP and Graphql apis ",
      icons: ["api-icon", "graphql-icon"]
    },
    {
      title: "Cloud Computing",
      desc:
        "Famailiarity with working in with AWS Services, Serverless Frameworks",
      icons: ["devicon-amazonwebservices-plain-wordmark", "google-cloud-icon"]
    },
    {
      title: "Continuous Integration and Continuous Deployment",
      desc:
        "Famailiarity with working in with CI/CD tools such as Jenkins, Docker e.t.c",
      icons: ["jenkins-icon", "devicon-docker-plain-wordmark"]
    },
    {
      title: "Responsive Design",
      desc:
        "Skilled in the combination of flexible grids, flexible images, and media queries to produce application that works well on any device"
    },
    {
      title: "508 Compliance",
      desc: "familiar with creating accessible applications"
    }
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
