import { Component, OnInit, Input } from '@angular/core';
import { Experience } from '../experience.component';


@Component({
  selector: 'kb-dev-experience-timeline',
  templateUrl: './experience-timeline.component.html',
  styleUrls: ['./experience-timeline.component.scss']
})
export class ExperienceTimelineComponent implements OnInit {
  @Input()  experiences: Experience[] = [];
  constructor() { }

  ngOnInit() {
  }

}
