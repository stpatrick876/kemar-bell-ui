import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
export interface Experience {
  id: string;
  title: string;
  description: string;
  org: {
    name: string;
    shortName?: string;
  };
  duration: {
    start: string;
    end: string;
  };
  skills?: string[];

}
@Component({
  selector: 'kb-dev-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {
  experiences: Experience[] = [
    {
      id: 'exp1',
      title: 'Student',
      description: 'Bachelors of Technology in Computer Information Systems',
      org: {
        name: 'New York City College of Technology',
        shortName: "NYCCT"
      },
      duration: {
         start: 'Jan 2012',
         end: 'Dec 2014'
      },
      skills: ['Database System Design', 'Web Page Design and Implementation', ' Java', 'JSP', 'Servlets']
    },
    {
      id: 'exp2',
      title: 'Intern Developer',
      description: 'Provided user requirements analysis, design and programming support for enhancement of intranet applications accessed by employees',
      org: {
        name: 'Mimedia',
        shortName: "Mimedia"
      },
      duration: {
         start: 'Jun 2014',
         end: 'Aug 2014'
      },
      skills: ['Java', 'Servlets', 'JSP', 'MySQL', 'Git']
    },
     {
      id: 'exp3',
      title: 'UI Developer',
      description: `Working alongside the design team implementing the designs, 
                            also developing custom solutions to address team necessities.`,
       org: {
         name: 'Gold Coast IT Solution',
         shortName: "GCIT"
       },
       duration: {
          start: 'June 2015',
          end: 'March 2017'
       },
       skills: ['ReactJS', 'Flux', 'MySql', 'NodeJS', 'HTML5', 'Bootstrap', 'Rest', 'jQuery', 'd3.js']
     }, {
      id: 'exp4',
      title: 'Application Engineer',
      description: `Working alongside the design team implementing the designs,
                            also developing custom solutions to address team necessities.`,
      org: {
        name: 'Arline Reporting Corporation',
        shortName: 'ARC'
      },
      duration: {
         start: 'March 2017',
         end: 'April 2019'
      },
      skills: ['Angularjs', 'Angular 2+', 'Grunt', 'Scss', 'HTML5', 'Bootstrap', 'SOAP Web Services',
       'REST', 'Git', 'CVS', 'Angular Material']
    }, {
      id: 'exp5',
      title: 'Software Engineer',
      description: `Working alongside the design team implementing the designs,
                            also developing custom solutions to address team necessities.`,
      org: {
        name: 'Fireye',
        shortName: 'FEYE'
      },
      duration: {
         start: 'April 2019',
         end: 'Present'
      },
      skills: [ 'Angular 2+', 'Grunt', 'Scss', 'HTML5', 'Bootstrap',
       'REST', 'Git', 'Angular Material']
    }
   ];
   isHandsetLayout: boolean;

  constructor(private breakpointObserver: BreakpointObserver) { }

  ngOnInit(): void {
    this.breakpointObserver.observe([
      Breakpoints.HandsetLandscape,
      Breakpoints.HandsetPortrait,
      Breakpoints.Tablet,
      Breakpoints.TabletLandscape,
      Breakpoints.TabletPortrait,
      Breakpoints.Small
    ]).subscribe(result => {
      this.isHandsetLayout = result.matches;
    });
  }

}
