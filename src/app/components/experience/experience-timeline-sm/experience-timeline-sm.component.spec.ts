import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperienceTimelineSmComponent } from './experience-timeline-sm.component';

describe('ExperienceTimelineSmComponent', () => {
  let component: ExperienceTimelineSmComponent;
  let fixture: ComponentFixture<ExperienceTimelineSmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExperienceTimelineSmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperienceTimelineSmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
