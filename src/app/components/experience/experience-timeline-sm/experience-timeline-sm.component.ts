import { Component, OnInit, Input } from '@angular/core';
import { Experience } from '../experience.component';

@Component({
  selector: 'kb-dev-experience-timeline-sm',
  templateUrl: './experience-timeline-sm.component.html',
  styleUrls: ['./experience-timeline-sm.component.scss']
})
export class ExperienceTimelineSmComponent implements OnInit {
  @Input()  experiences: Experience[] = [];

  constructor() { }

  ngOnInit(): void {
  }

}
