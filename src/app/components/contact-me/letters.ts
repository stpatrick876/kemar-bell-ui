import { formStep } from './contact-me-form/contact-me-form.component';

export class FlyingLetters {


    constructor() {

    }

    initializeFlyingLetters(step: formStep) {
      const canvas: any = document.getElementById('canvas');
      const ctx = canvas.getContext('2d');

      let grd;
      let keysDown = [];
      const letters = [];

      const symbols = [{ k: 81, s: 'q', x: 5 }, { k: 87, s: 'w', x: 15 }, { k: 69, s: 'e', x: 25 }, { k: 82, s: 'r', x: 35 },
       { k: 84, s: 't', x: 45 }, { k: 89, s: 'y', x: 55 }, { k: 85, s: 'u', x: 65 }, { k: 73, s: 'i', x: 75 },
       { k: 79, s: 'o', x: 85 }, { k: 80, s: 'p', x: 95 }, { k: 65, s: 'a', x: 10 }, { k: 83, s: 's', x: 20 },
       { k: 68, s: 'd', x: 30 }, { k: 70, s: 'f', x: 40 }, { k: 71, s: 'g', x: 50 }, { k: 72, s: 'h', x: 60 },
       { k: 74, s: 'j', x: 70 }, { k: 75, s: 'k', x: 80 }, { k: 76, s: 'l', x: 90 }, { k: 90, s: 'z', x: 20 },
       { k: 88, s: 'x', x: 30 }, { k: 67, s: 'c', x: 40 }, { k: 86, s: 'v', x: 50 }, { k: 66, s: 'b', x: 60 },
       { k: 78, s: 'n', x: 70 }, { k: 77, s: 'm', x: 80 }, { k: 48, s: '0', x: 90 }, { k: 49, s: '1', x: 0 },
       { k: 50, s: '2', x: 10 }, { k: 51, s: '3', x: 20 }, { k: 52, s: '4', x: 30 }, { k: 53, s: '5', x: 40 },
        { k: 54, s: '6', x: 50 }, { k: 55, s: '7', x: 60 }, { k: 56, s: '8', x: 70 }, { k: 57, s: '9', x: 80 }];

      function Letter(key: number) {
        this.x = findX(key);
        this.symbol = findS(key);
        this.color = 'rgba(255, 255, 255, ' + Math.random() + ')';
        this.size = Math.floor((Math.random() * 40) + 12);
        this.path = getRandomPath(this.x);
        this.rotate = Math.floor((Math.random() * Math.PI) + 1);
        this.percent = 0;
      }

      Letter.prototype.draw = function() {
        const percent = this.percent / 100;
        const xy = getQuadraticBezierXYatPercent(this.path[0], this.path[1], this.path[2], percent);
        ctx.save();
        ctx.translate(xy.x, xy.y);
        ctx.rotate(this.rotate);
        ctx.font = this.size + 'px Arial';
        ctx.fillStyle = this.color;
        ctx.fillText(this.symbol, -15, -15);
        ctx.restore();
      };

      Letter.prototype.drawPath = function() {
        ctx.lineWidth = 1;
        ctx.beginPath();
        ctx.moveTo(this.path[0].x, this.path[0].y);
        ctx.quadraticCurveTo(this.path[1].x, this.path[1].y, this.path[2].x, this.path[2].y);
        ctx.stroke();
      };

      function findX(key) {
        for (const symbol of symbols) {
          if (symbol.k === key) {
            return (symbol.x * canvas.width / 100);
          }
        }
        return false;
      }

      function findS(key) {
        for (const symbol of symbols) {
          if (symbol.k === key) {
            return symbol.s;
          }
        }
        return false;
      }

      function getRandomPath(x) {
        const xStart = x;
        const xEnd = xStart + Math.floor((Math.random() * 400) - 199);

        return [{
          x: xStart,
          y: canvas.height
        }, {
          x: (xStart + xEnd) / 2,
          y: Math.floor((Math.random() * canvas.height) - canvas.height)
        }, {
          x: xEnd,
          y: canvas.height
        }];
      }

      function drawBackground() {
        ctx.fillStyle = grd;
        ctx.fillRect(0, 0, canvas.width, canvas.height);
      }


      function getQuadraticBezierXYatPercent(startPt, controlPt, endPt, percent) {
        const x = Math.pow(1 - percent, 2) * startPt.x + 2 * (1 - percent) * percent * controlPt.x + Math.pow(percent, 2) * endPt.x;
        const y = Math.pow(1 - percent, 2) * startPt.y + 2 * (1 - percent) * percent * controlPt.y + Math.pow(percent, 2) * endPt.y;
        return ({ x, y });
      }


      function resize() {
        const box = canvas.getBoundingClientRect();
        canvas.width = box.width;
        canvas.height = box.height;
        grd = ctx.createRadialGradient(canvas.width / 2, canvas.height / 2, 0, canvas.width / 2, canvas.height / 2, canvas.height);
        // grd.addColorStop(0, '#ffc300');
        // grd.addColorStop(1, '#ffc300');
      }

      function draw() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        drawBackground();

        for (let i = 0; i < letters.length; i++) {
          letters[i].percent += 1;
          letters[i].draw();
          // letters[i].drawPath();
          if (letters[i].percent > 100) {
            letters.splice(i, 1);
          }
        }

        for (let i = 0; i < keysDown.length; i++) {
          if (keysDown[i]) {
            letters.push(new Letter(i));
          }
        }
        requestAnimationFrame(draw);
      }
      const startKeys = [81, 87, 69, 82, 84, 89, 85, 73, 79, 80];

      function startAnimation() {
        setTimeout(() => {
          const key = startKeys.pop();
          keysDown[key] = true;
          setTimeout(() => {
            keysDown[key] = false;
          }, 180);
          if (startKeys.length > 0) {
            startAnimation();
          }
        }, 180);
      }

      function setupKeyListeners() {
        const el =  document.getElementById(`${step}Input`);
        if (!el) {
          return false;
        }
        el.onkeyup = (event) => {
          keysDown[event.keyCode] = false;
        };

        el.onkeydown = (event) => {
          if (event.keyCode === 91 || event.keyCode === 224) {
            keysDown = [];
          } else if (event.keyCode >= 65 && event.keyCode <= 90 || event.keyCode >= 48 && event.keyCode <= 57) {
            keysDown[event.keyCode] = true;
          }
        };

      }
      resize();
      draw();
      startAnimation();

      window.onresize = resize;
      setupKeyListeners();
      // window.requestAnimationFrame = (function () {
      //   return window.requestAnimationFrame ||
      //     window.webkitRequestAnimationFrame ||
      //     window.mozRequestAnimationFrame ||
      //     window.oRequestAnimationFrame ||
      //     window.msRequestAnimationFrame ||
      //     function (callback) {
      //       window.setTimeout(callback, 1000 / 60);
      //     };
      // })();
    }

}
