import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { FlyingLetters } from '../letters';

export enum formStep {
  NAME = 'name',
  EMAIL = 'email',
  MESSAGE = 'message'
}
interface IFormField {
  index: number;
  step: formStep;
}
@Component({
  selector: 'kb-dev-contact-me-form',
  templateUrl: './contact-me-form.component.html',
  styleUrls: ['./contact-me-form.component.scss']
})
export class ContactMeFormComponent implements OnInit, AfterViewInit {
  formStep = formStep;
  fields: IFormField[] = [{
    index: 1,
    step: formStep.NAME
  }, {
    index: 2,
    step: formStep.EMAIL
  }, {
    index: 3,
    step: formStep.MESSAGE
  }];
  activeStep: IFormField = this.fields[0];
  contactForm: FormGroup;
  showSuccess = false;
  fl: FlyingLetters = new FlyingLetters();
  constructor(private fb: FormBuilder, private http: HttpClient) {}

  ngOnInit() {
    this.contactForm = this.fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required]
    });
  }

  ngAfterViewInit() {
    this.fl.initializeFlyingLetters(formStep.NAME);
  }

  isStepEnabled(step: formStep): boolean {
    let enabled = true;
    switch (step) {
     case formStep.EMAIL:
     enabled = this.contactForm.get('name').valid;  break;
     case formStep.MESSAGE:
     enabled = this.contactForm.get('email').valid;  break;
     default:
       break;
   }
    return enabled;
  }
  isStepActive(step: formStep): boolean {
     return this.activeStep === this.fields.filter(f => f.step === step)[0];
  }


  goToStep(step: formStep): void {
    if (this.isStepEnabled(step)) {
      this.activeStep = this.fields.filter(f => f.step === step)[0];
      setTimeout(() => {
        this.fl.initializeFlyingLetters(step);
      });

    }
  }

  onSubmit() {
    if (this.contactForm.invalid) {
      // TODO: add validation notification
      return false;
    }

    const { name, email, message } = this.contactForm.value;

    this.http
      .post(
        'https://m0d4uz2wb6.execute-api.us-east-1.amazonaws.com/dev/email/send/json',
        {
          name,
          email,
          content: message
        }
      )
      .subscribe((res: any) => {
        if (res.MessageId) {
          this.showSuccess = true;
          setTimeout(() => {
            this.showSuccess = false;
            this.contactForm.reset();
            this.goToStep(formStep.NAME);
          }, 5000);
        }
      });
  }
}
