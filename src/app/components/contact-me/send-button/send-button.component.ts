import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'kb-dev-send-button',
  templateUrl: './send-button.component.html',
  styleUrls: ['./send-button.component.scss']
})
export class SendButtonComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.initializeSendButton();

  }

  initializeSendButton() {
    $(document).ready(() => {
      const $button = $('button[type="submit"]');

      $button.on('click', function() {
        const $this = $(this);
        if ($this.hasClass('active') || $this.hasClass('success')) {
          return false;
        }
        $this.addClass('active');
        setTimeout(() => {
          $this.addClass('loader');
        }, 125);
        setTimeout(() => {
          $this.removeClass('loader active');
          $this.text('Sent');
          $this.addClass('success animated pulse');
        }, 1600);
        setTimeout(() => {
          $this.text('Send');
          $this.removeClass('success animated pulse');
          $this.blur();
        }, 2900);
      });
    });
  }



}
