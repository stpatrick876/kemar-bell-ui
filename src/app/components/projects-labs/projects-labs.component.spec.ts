import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectsLabsComponent } from './projects-labs.component';

describe('ProjectsLabsComponent', () => {
  let component: ProjectsLabsComponent;
  let fixture: ComponentFixture<ProjectsLabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectsLabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectsLabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
