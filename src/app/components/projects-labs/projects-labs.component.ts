import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'kb-dev-projects-labs',
  templateUrl: './projects-labs.component.html',
  styleUrls: ['./projects-labs.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class ProjectsLabsComponent implements OnInit {
  projects: any[] = [
    {
      num: 1,
      title: `Spotify Lite`,
      description: 'Music player, clone of Spotify',
      resources: [
        'Angular 7',
        'angular material',
        'nodeJs',
        'graphQl(Apollo)',
        'mysql',
        'Scss'
      ]
    },
    {
      num: 2,
      title: `Connect Four`,
      resources: [
        'Angular 2',
        'RxJs',
        'bootstrap 4',
        'angular material',
        'protractor',
        'jasmine'
      ],
      screenShot: {
        url: 'assets/img/Connect4.png',
        caption: 'Connect 4 Menu'
      },
      repoUrl: 'https://gitlab.com/stpatrick876/connect-4.git',
      demoUrl: 'http://connect-4-kstb.s3-website-us-east-1.amazonaws.com/',
      description: 'Single Player connect 4 game'
    },
    {
      num: 3,
      title: `IO Chat`,
      resources: [
        'Angular 5',
        'RxJs',
        'nodeJs',
        'bootstrap 4',
        'angular material',
        'auth0',
        'socket io'
      ],
      repoUrl: 'https://gitlab.com/kem-sou/simple-crud-app',
      description: 'Chat rooms app '
    }
  ];
  skills: string[] = ['Javascript', 'HTML', 'Scss', 'Typescript', 'Angular',
                      'ReactJs', 'VueJs', 'Nodejs', 'D3js', 'ExpressJs'];
  constructor() { }

  ngOnInit(): void {

  }

}
