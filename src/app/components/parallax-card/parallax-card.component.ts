import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'kb-dev-parallax-card',
  templateUrl: './parallax-card.component.html',
  styleUrls: ['./parallax-card.component.scss']
})
export class ParallaxCardComponent implements OnInit {
  @Input() project: any;

  constructor() { }

  ngOnInit(): void {
  }

}
