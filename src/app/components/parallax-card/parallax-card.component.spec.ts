import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParallaxCardComponent } from './parallax-card.component';

describe('ParallaxCardComponent', () => {
  let component: ParallaxCardComponent;
  let fixture: ComponentFixture<ParallaxCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParallaxCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParallaxCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
