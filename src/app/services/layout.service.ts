import { Injectable } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {

  constructor(private breakpointObserver: BreakpointObserver) {
    
   }
}
