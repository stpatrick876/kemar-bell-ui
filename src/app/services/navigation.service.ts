import { Injectable } from '@angular/core';
import { LoaderService } from './loader.service';
import { Router } from '@angular/router';
export interface INavItem {
  label: string;
  path: string;
  icon?: string;
}
@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  static navItems: INavItem[] =  [{
    label: 'Home',
    path: 'home',
    icon: 'fa fa-home fa-2x'
  },
  {
    label: 'About Me',
    path: 'aboutMe',
    icon: 'fa fa-user fa-2x'
  }, {
    label: 'Experience',
    path: 'experience',
    icon: 'fa fa-briefcase fa-2x'
  }, {
    label: 'Projects/Labs',
    path: 'demos',
    icon: 'fa fa-laptop fa-2x'
  }, {
    label: 'Contact Me',
    path: 'contact',
    icon: 'fa fa-phone fa-2x'
  }];

  constructor(private router: Router, private loader: LoaderService) { }

  navigateTo(path: string) {
    this.loader.show();
    this.router.navigate([`/${path}`]);
    setTimeout(() => {
      this.loader.hide();
    }, 3000);
  }
}
