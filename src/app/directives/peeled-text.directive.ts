import { Directive, ElementRef, OnInit, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[kbDevPeeledText]'
})
export class PeeledTextDirective implements AfterViewInit {

  constructor(private el: ElementRef) { }

  ngAfterViewInit() {
    let content = this.el.nativeElement.textContent;
    const chars = content.split('');
    content = chars.map((char: string) => {
      const c = char !== ' ' ? `<span class="peeled-text__char" data-text="${char}">${char}</span>` : char;

      return c;
    }).join('');
    this.el.nativeElement.innerHTML = content;
  }
}
