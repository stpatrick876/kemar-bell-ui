import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[kbDevAnimatedChars]'
})
export class AnimatedCharsDirective implements OnInit {

  constructor(private el: ElementRef) { }

  ngOnInit() {
    let content = this.el.nativeElement.textContent;
    const chars = content.split('');
    content = chars.map((char: string) => {
      const c = char !== ' ' ? `<span class="animated">${char}</span>` : char;

      return c;
    }).join('');
    this.el.nativeElement.innerHTML = content;
  }

}
