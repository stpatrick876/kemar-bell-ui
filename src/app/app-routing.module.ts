import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SplashComponent } from './components/splash/splash.component';
import { AboutMeComponent } from './components/about-me/about-me.component';
import { ExperienceComponent } from './components/experience/experience.component';
import { ProjectsLabsComponent } from './components/projects-labs/projects-labs.component';
import { ContactMeComponent } from './components/contact-me/contact-me.component';
import { HomeComponent } from './components/home/home.component';


const routes: Routes = [
  {
    path: '',
    component: SplashComponent
  },
  {
    path: 'home',
    component: HomeComponent,
    data: {animation: 'Home'}
  },
  {
    path: 'aboutMe',
    component: AboutMeComponent,
    data: {animation: 'Home'}
  },
  {
    path: 'experience',
    component: ExperienceComponent,
    data: {animation: 'Home'}
  },
  {
    path: 'demos',
    component: ProjectsLabsComponent,
    data: {animation: 'Home'}
  },
  {
    path: 'contact',
    component: ContactMeComponent,
    data: {animation: 'Home'}
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule { }
