import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SplashComponent } from './components/splash/splash.component';
import { AboutMeComponent } from './components/about-me/about-me.component';
import { ExperienceComponent } from './components/experience/experience.component';
import { ProjectsLabsComponent } from './components/projects-labs/projects-labs.component';
import { ContactMeComponent } from './components/contact-me/contact-me.component';
import { AppComponent } from './components/app/app.component';
import { AnimatedCharsDirective } from './directives/animated-chars.directive';
import { CubeOfTechComponent } from './components/about-me/cube-of-tech/cube-of-tech.component';
import { TickerOfTechComponent } from './components/about-me/ticker-of-tech/ticker-of-tech.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SendButtonComponent } from './components/contact-me/send-button/send-button.component';
import { LocationMapComponent } from './components/contact-me/location-map/location-map.component';
import { AgmCoreModule } from '@agm/core';
import { HomeComponent } from './components/home/home.component';
import { NameGraphicComponent } from './components/name-graphic/name-graphic.component';
import { ExperienceTimelineComponent } from './components/experience/experience-timeline/experience-timeline.component';
import { NewsTickerComponent } from './components/experience/news-ticker/news-ticker.component';
import { ExperienceTimelineSmComponent } from './components/experience/experience-timeline-sm/experience-timeline-sm.component';
import {ScrollingModule} from '@angular/cdk/scrolling';
import { LoaderComponent } from './components/loader/loader.component';
import { HoverButtonComponent } from './components/hover-button/hover-button.component';
import { ParallaxCardComponent } from './components/parallax-card/parallax-card.component';
import { LoaderService } from './services/loader.service';
import { ContactMeFormComponent } from './components/contact-me/contact-me-form/contact-me-form.component';
import { SocialButtonComponent } from './components/contact-me/social-button/social-button.component';
import { ToggleNavComponent } from './components/toggle-nav/toggle-nav.component';
import { PeeledTextDirective } from './directives/peeled-text.directive';

@NgModule({
  declarations: [
    AppComponent,
    SplashComponent,
    AboutMeComponent,
    ExperienceComponent,
    ProjectsLabsComponent,
    ContactMeComponent,
    AnimatedCharsDirective,
    CubeOfTechComponent,
    TickerOfTechComponent,
    SendButtonComponent,
    LocationMapComponent,
    HomeComponent,
    NameGraphicComponent,
    ExperienceTimelineComponent,
    NewsTickerComponent,
    ExperienceTimelineSmComponent,
    LoaderComponent,
    HoverButtonComponent,
    ParallaxCardComponent,
    ContactMeFormComponent,
    ToggleNavComponent,
    SocialButtonComponent,
    PeeledTextDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyC2PyMiPnvGyqe-Jv2Nkd7krWuPUH0OWvQ'}),
    ScrollingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

